package com.neology.soat_mifare;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.nfc.TagLostException;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neology.soat_mifare.dialogs.ConfirmRecord_Dialog;
import com.neology.soat_mifare.dialogs.Dialog_Settings_ip;
import com.neology.soat_mifare.dialogs.Settings_Dialog;
import com.neology.soat_mifare.utils.CheckInternetConnection;
import com.neology.soat_mifare.utils.Constants;
import com.neology.soat_mifare.utils.Read_UID_Doc;
import com.neology.soat_mifare.utils.SnackBar;
import com.nxp.nfclib.classic.IMFClassic;
import com.nxp.nfclib.classic.IMFClassicEV1;
import com.nxp.nfclib.exceptions.CloneDetectedException;
import com.nxp.nfclib.exceptions.ReaderException;
import com.nxp.nfclib.exceptions.SmartCardException;
import com.nxp.nfclib.ntag.INTag;
import com.nxp.nfclib.ntag.INTag213215216;
import com.nxp.nfclib.utils.NxpLogUtils;
import com.nxp.nfclib.utils.Utilities;
import com.nxp.nfcliblite.NxpNfcLibLite;
import com.nxp.nfcliblite.Nxpnfcliblitecallback;
import com.nxp.nfcliblite.cards.IPlus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public static String TAG = MainActivity.class.getSimpleName();
    private NxpNfcLibLite libInstance = null;
    public static final int READER_TIME_OUT = 1500;

    private Toolbar mToolbar;
    private CountDownTimer mCountDownTimer;

    Resources res;
    SharedPreferences sharedPreferences;

    String[] datos, folios, marcas, submarcas, placas, modelos, fechaExp, fechaEmi, polizas = null;

    private TextView tvUI;
    private ImageView ivUI;
    private RelativeLayout rlUI;
    LinearLayout datosNFC, vencimientoLinearLayOut;

    MenuItem search_itemUI;
    private Menu menu_mainUI;

    private String no_cedula = "", tipoPoliza = "", polizaValida = "";
    private boolean datosValidos = false;
    boolean resultadoLectura = false;

    private ProgressDialog dialog;
    /**
     * Mifare MFClassic instance initiated.
     */
    private IMFClassic classic;
    /**
     * Mifare Plus instance initiated.
     */
    private IPlus plus;

    private enum EnumCardType {
        EnumPlus,
        EnumNTag213215216,
        EnumClassicEV1,
        EnumNone
    }

    private EnumCardType mCardType = EnumCardType.EnumNone;

    private boolean mIsPerformingCardOperations = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_NAME, Context.MODE_PRIVATE);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("SOAT");
        mToolbar.setTitleTextColor(Color.BLUE);
        mToolbar.setNavigationIcon(R.mipmap.logo);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        libInstance = NxpNfcLibLite.getInstance();
        // Call registerActivity function before using other functions of the library.
        libInstance.registerActivity(this);

        InputStream uid_doc = getResources().openRawResource(R.raw.uid);
        Read_UID_Doc.createJsonStructure(uid_doc);

        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_label));
        dialog.setTitle(getString(R.string.gettingdata_label));
        dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showDialog();
        } else if(id == R.id.action_settings_ip) {
            showDialogIP();
        } else if (id == R.id.action_search) {
            if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
                if (!no_cedula.equals("")) {
                    openData();
                } else {
                    Toast.makeText(getApplicationContext(), "DEBE LEER UN TAG PRIMERO", Toast.LENGTH_SHORT).show();
                }
            } else {
                SnackBar.showSnackBar(getResources().getString(R.string.no_internet), this, 1);
            }

        }

        return true;
    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }

    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        dialog.show();
        resultadoLectura = false;
        if (intent != null) {
            try {
                libInstance.filterIntent(intent, new Nxpnfcliblitecallback() {
                    @Override
                    public void onClassicEV1CardDetected(final IMFClassicEV1 imfClassicEV1) {
                        if (mCardType == EnumCardType.EnumClassicEV1 && mIsPerformingCardOperations) {
                            //Already Some Operations are happening in the same card, discard the callback
                            Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                            return;
                        }
                        mIsPerformingCardOperations = true;
                        mCardType = EnumCardType.EnumClassicEV1;


                        classic = imfClassicEV1;
                /* Insert your logic here by commenting the function call below. */
                        try {
                            classic.getReader().connect();
                            classicEV1CardLogic();
                        } catch (Throwable t) {
                            Log.e(TAG, "Unknown Error Tap Again!");
                        }

                        mIsPerformingCardOperations = false;
                    }

                    @Override
                    public void onPlusCardDetected(final IPlus iPlus) {
                        if (mCardType == EnumCardType.EnumPlus && mIsPerformingCardOperations) {
                            //Already Some Operations are happening in the same card, discard the callback
                            Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                            return;
                        }
                        mIsPerformingCardOperations = true;
                        mCardType = EnumCardType.EnumPlus;

                        plus = iPlus;
                        try {
                            plus.getReader().connect();
//                        plusCardLogic();
                            if (Utilities.dumpBytes(plus.getCardDetails().uid) != null) {
                                Log.i(TAG, "Card Detected: " + plus.getCardDetails().cardName + " "
                                        + plus.getCardDetails().securityLevel + Utilities.dumpBytes(plus.getCardDetails().uid));
                                getSharedUid(Utilities.dumpBytes(plus.getCardDetails().uid));
                                resultadoLectura = true;
                                dialog.dismiss();
                            } else {
                                Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                            }

                            plus.getReader().close();
                        } catch (ReaderException e1) {
                            e1.printStackTrace();
                            Log.e(TAG, "Unknown Error Tap Again!");
                            Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                        } catch (Throwable t) {
                            t.printStackTrace();
                            Log.e(TAG, "Unknown Error Tap Again!");
                            Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                        }
                        mIsPerformingCardOperations = false;

                    }

                    @Override
                    public void onNTag213215216CardDetected(final INTag213215216 inTag213215216) {
                        if (mCardType == EnumCardType.EnumNTag213215216 && mIsPerformingCardOperations) {
                            //Already Some Operations are happening in the same card, discard the callback
                            Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                            return;
                        }
                        mIsPerformingCardOperations = true;
                        mCardType = EnumCardType.EnumNTag213215216;

                        try {
                            inTag213215216.getReader().connect();
                            ntagCardLogic(inTag213215216);
                        } catch (ReaderException e) {

                            e.printStackTrace();
                        }


                        mIsPerformingCardOperations = false;
                    }
                });
            } catch (CloneDetectedException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Intentar de nuevo", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "Intentar de nuevo", Toast.LENGTH_SHORT).show();
        }

        if (!resultadoLectura) {
            Toast.makeText(getApplicationContext(), "Intentar lectura de nuevo", Toast.LENGTH_SHORT).show();
        }
        dialog.dismiss();
    }

    /**
     * Plus lite operations.
     *
     * @throws SmartCardException when exception occur.
     */
    public void plusCardLogic() throws SmartCardException {
        int i = 0;
        try {
            Log.i(TAG, "Card Detected: " + plus.getCardDetails().cardName + " "
                    + plus.getCardDetails().securityLevel + Utilities.dumpBytes(plus.getCardDetails().uid));
            i++;
            //No se porque se ejecuta dos veces la lectura en el PLus, asi que a la 2da vuelta traemos el método que me interesa
//            if(i==2) {
//                getSharedUid(Utilities.dumpBytes(plus.getCardDetails().uid));
//            }
            getSharedUid(Utilities.dumpBytes(plus.getCardDetails().uid));
            plus.getReader().close();
        } catch (ReaderException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Mifare classic Card Logic.
     *
     * @throws SmartCardException
     */
    public void classicEV1CardLogic() throws SmartCardException {
        Log.i(TAG, "Card Detected : " + classic.getCardDetails().cardName);

        try {
            Log.i(TAG, "Uid :" + Utilities.dumpBytes(classic.getUID()));
            getSharedUid(Utilities.dumpBytes(classic.getUID()));
            dialog.dismiss();
            resultadoLectura = true;
            classic.getReader().close();
        } catch (ReaderException e) {

            e.printStackTrace();
        }
    }

    /**
     * Ntag Operations are, getTagname(), getUID(), Write and Read.
     *
     * @param tag object
     */
    private void ntagCardLogic(final INTag tag) {
        Log.i(TAG, "Card Detected : " + tag.getTagName());

        try {
            NxpLogUtils.d(TAG, "testBasicNtagFunctionality, start");

            Log.i(TAG, "UID of the Tag: " + Utilities.dumpBytes(tag.getUID()));
            getSharedUid(Utilities.dumpBytes(tag.getUID()));
            dialog.dismiss();
            resultadoLectura = true;
            tag.getReader().close();
            NxpLogUtils.d(TAG, "testBasicNtagFunctionality, End");
        } catch (TagLostException e) {
            Log.e(TAG, "TagLost Exception - Tap Again!");
            e.printStackTrace();
        } catch (IOException e) {

            Log.e(TAG, "IO Exception -  Check logcat!");
            e.printStackTrace();
        } catch (SmartCardException e) {
            Log.e(TAG, "SmartCard Exception - Check logcat!");
            e.printStackTrace();
        } catch (Throwable t) {
            Log.e(TAG, "Exception - Check logcat!");
            t.printStackTrace();
        }
    }

    public void showDialog() {
        DialogFragment dialogFragment = Settings_Dialog.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void showDialogIP() {
        DialogFragment dialogFragment = Dialog_Settings_ip.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog_ip");
    }

    private void read_UIDjson(JSONArray array, String UID) {
        boolean resultado = false;
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                String uid = jsonObject.getString("uid");
                String folio = jsonObject.getString("folio");
                Log.d(TAG, "UID JSON " + uid);
                Log.d(TAG, "POLIZA JSON " + folio);
                if (uid.equals(UID)) {
                    //mostrar contenido
                    loadDataOffLine(folio);
                    resultado = true;
                    break;
                } else {
                    resultado = false;
                }
            }
        } catch (Exception e) {

        }
        if (!resultado) {
            showPersoDialog();
        }
    }

    public void showPersoDialog() {
        DialogFragment dialogFragment = ConfirmRecord_Dialog.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void getSharedUid(String UID) {
        if (!sharedPreferences.contains(UID)) {
            //buscar en json definido
            read_UIDjson(Read_UID_Doc.uid_item, UID);
        } else {
            //mostrar contenido
            String[] elementos = sharedPreferences.getString(UID, null).split("\\|");
//            String uid_tag = elementos[0];
//            String tipoPoliza = elementos[1];
//            String polizaValida = "OK";
            loadDataOffLine(elementos[1]);
        }
    }

    private void loadDataOffLine(String folio) {
        res = getResources();
        folios = res.getStringArray(R.array.folios);
        marcas = res.getStringArray(R.array.marcas);
        submarcas = res.getStringArray(R.array.submarcas);
        placas = res.getStringArray(R.array.placas);
        modelos = res.getStringArray(R.array.anio_modelo);
        fechaExp = res.getStringArray(R.array.fecha_exp);
        fechaEmi = res.getStringArray(R.array.fecha_emision);
        polizas = res.getStringArray(R.array.polizas);

//        09-14 11:26:30.730: D/MainActivity(21483): Dato[0]: 85923797A-12
//        09-14 11:26:30.730: D/MainActivity(21483): Dato[1]: A12345-8
//        09-14 11:26:30.730: D/MainActivity(21483): Dato[2]: 26-07-2013
//        09-14 11:26:30.730: D/MainActivity(21483): Dato[3]: 26-07-2014
//        09-14 11:26:30.730: D/MainActivity(21483): Dato[4]: 1
//        09-14 11:26:30.730: D/MainActivity(21483): Dato[5]: 1

//        for (int i = 0; i < folios.length; i++) {
//            if (folio.equals(folios[i])) {
//                showInfo(folios[i], placas[i], fechaExp[i], fechaEmi[i], "1", polizas[i]);
//                break;
//            }
//        }

        String tipoPoliza[] = folio.split("_");
        if (tipoPoliza[0].equals("P")) {
            showInfo(tipoPoliza[1], placas[0], fechaExp[0], fechaEmi[0], "1", polizas[0], folio);
        } else if (tipoPoliza[0].equals("T")) {
            showInfo(tipoPoliza[1], placas[1], fechaExp[1], fechaEmi[1], "1", polizas[1], folio);
        } else if (tipoPoliza[0].equals("M")) {
            showInfo(tipoPoliza[1] + " " + tipoPoliza[2], placas[2], fechaExp[2], fechaEmi[2], "1", polizas[2], folio);
        }
    }

    private void showInfo(String... datosTag) {
        try {

            LinearLayout l = (LinearLayout) findViewById(R.id.bienvenida_msg);
            l.setVisibility(View.GONE);

            vencimientoLinearLayOut = (LinearLayout) findViewById(R.id.vencimientoLayOutID);
            vencimientoLinearLayOut.setVisibility(View.VISIBLE);

            datosNFC = (LinearLayout) findViewById(R.id.datosNFClLayOutID);

            if (datosTag != null) {

                Log.d(TAG, "# Datos leidos: " + datosTag.length);
                int i = 0;
                while (i < datosTag.length) {
                    Log.d(TAG, "Dato[" + i + "]: " + datosTag[i]);
                    i++;
                }

                tvUI = (TextView) findViewById(R.id.estado);
//				ivUI = (ImageView) findViewById(R.id.estado_img);

                try {
                    Calendar c = Calendar.getInstance();


                    SimpleDateFormat outFormat = new SimpleDateFormat(
                            "DD/MM/yyyy", Locale.getDefault());


                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    Date date_vencimiento = df.parse(datosTag[3]);
                    System.out.println(date_vencimiento);

                    Date hoy = c.getTime();
                    datosTag[2] = datosTag[2].replace("-", "/");
                    datosTag[3] = datosTag[3].replace("-", "/");

                    if (hoy.before(date_vencimiento)) {
                        Log.d(TAG,
                                "Permiso OK: "
                                        + outFormat.format(date_vencimiento));
//						tvUI.setText(R.string.status_ok);
                        polizaValida = "OK";
//						ivUI.setImageResource(R.drawable.ok);
                    } else {
                        Log.d(TAG,
                                "Permiso Vencido: "
                                        + outFormat.format(date_vencimiento));
//						tvUI.setText(R.string.status_expired);
                        polizaValida = "VENCIDA";
//						ivUI.setImageResource(R.drawable.vencido);
                    }

                } catch (Exception e) {
                    Log.e(TAG,
                            "Exception::lecturaNFC:Bloque fecha->"
                                    + e.getMessage());
                }

                no_cedula = datosTag[6];
                tvUI = (TextView) findViewById(R.id.matricula);
                tvUI.setText(datosTag[0]);
                tvUI = (TextView) findViewById(R.id.placaTxt);
                tvUI.setText(datosTag[1]);
//				tvUI = (TextView) findViewById(R.id.fecha_emision);
//				tvUI.setText(datosTag[2]);
//				tvUI = (TextView) findViewById(R.id.fecha_vencimiento);
//				tvUI.setText(datosTag[3]);

                int indice_depto = Integer.parseInt(datosTag[4]);

                Resources res = getResources();
                String[] departamentos = null;
                departamentos = res.getStringArray(R.array.departamentos);

                TextView LUGAReMISIONtXT = (TextView) findViewById(R.id.lugar_emision);
                LUGAReMISIONtXT.setText(departamentos[indice_depto]);

                int id_img_depto = 0;

                TextView tipoPolizaTxt = (TextView) findViewById(R.id.tipoPolizaTxt);

                tipoPoliza = datosTag[5];
                switch (Integer.parseInt(datosTag[5])) {
                    case 1:
                        id_img_depto = getResources().getIdentifier("soat5",
                                "drawable", getPackageName());
                        tipoPolizaTxt.setText("Particular");
                        LUGAReMISIONtXT.setText("COCHABAMBA");
                        break;

                    case 2:
                        id_img_depto = getResources().getIdentifier("soat4",
                                "drawable", getPackageName());
                        tipoPolizaTxt.setText("Motocicleta");
                        LUGAReMISIONtXT.setText("CHUQUISACA");
                        break;

                    case 3:
                        id_img_depto = getResources().getIdentifier("verde_1",
                                "drawable", getPackageName());
                        tipoPolizaTxt.setText("Transporte Público");
                        LUGAReMISIONtXT.setText("SANTA CRUZ");
                        break;
                }

                ivUI = (ImageView) findViewById(R.id.img_depto);
                ivUI.setImageResource(id_img_depto);

//                try {
//                    search_itemUI = menu_mainUI.findItem(R.id.buscar_item);
//                    search_itemUI.setIcon(R.drawable.search);
//                } catch (NullPointerException e) {
//                    Log.e(TAG, "No hay menu aun: " + e.getMessage());
//                }

                datosValidos = true;
                datosNFC.setVisibility(View.VISIBLE);

            } else {

                Log.i(TAG, "Lectura de datos no validos");

                ivUI = (ImageView) findViewById(R.id.estado_img);
                ivUI.setImageResource(R.drawable.error);

                tvUI = (TextView) findViewById(R.id.estado);
                tvUI.setText(R.string.status_invalid);

//                try {
//                    search_itemUI = menu_mainUI.findItem(R.id.buscar_item);
//                    search_itemUI.setIcon(R.drawable.search);
//                } catch (NullPointerException e) {
//                    Log.e(TAG, "No hay menu aun: " + e.getMessage());
//                    e.printStackTrace();
//                }

                datosValidos = false;
                rlUI.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Excepion::lecturaNFC->" + e.getMessage());
            e.printStackTrace();
        }

    }

    public void openData() {
        Intent intent = new Intent(this, DataActivity.class);
        intent.putExtra("NO_CEDULA", no_cedula + "|" + tipoPoliza + "|" + polizaValida);
        startActivity(intent);
    }
}
