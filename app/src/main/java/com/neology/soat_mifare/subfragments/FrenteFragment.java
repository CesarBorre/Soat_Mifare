package com.neology.soat_mifare.subfragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neology.soat_mifare.DataActivity;
import com.neology.soat_mifare.R;
import com.neology.soat_mifare.VolleyApp;
import com.neology.soat_mifare.utils.DateUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class FrenteFragment extends Fragment {

    Context context;

    TextView fechaVen;
    TextView poliza;
    ImageView img;
    RelativeLayout layOutValid;
    String fechaV = "";

    int iConta = 0;
    private static final String URL = "http://mobile.neology-demos.com:8080/api/insertNewDateAutosSoat";

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_frente,
                container, false);
        context = (Context) getArguments().get("");

        ViewHolder viewHolder = new ViewHolder();

        viewHolder.contenedor = (LinearLayout) rootView.findViewById(R.id.contenedorPrincipal);
        viewHolder.foto = (ImageView) rootView.findViewById(R.id.foto_img);
        viewHolder.cedulaLbl = (TextView) rootView.findViewById(R.id.lblCedulaID);
        viewHolder.cedulaTxt = (TextView) rootView.findViewById(R.id.txtCedulaID);
        viewHolder.nombreLbl = (TextView) rootView.findViewById(R.id.label_nombre);
        viewHolder.nombreTxt = (TextView) rootView.findViewById(R.id.nombre);
        viewHolder.apellidosLbl = (TextView) rootView.findViewById(R.id.label_apellidos);
        viewHolder.apellidosTxt = (TextView) rootView.findViewById(R.id.apellidos);
        viewHolder.profesionLbl = (TextView) rootView.findViewById(R.id.label_profesion);
        viewHolder.profesionTxt = (TextView) rootView.findViewById(R.id.profesion);
        viewHolder.estadoCivilLbl = (TextView) rootView.findViewById(R.id.label_estado_civil);
        viewHolder.estadoCivilTxt = (TextView) rootView.findViewById(R.id.estado_civil);
        viewHolder.fechaNacLbl = (TextView) rootView.findViewById(R.id.label_fecha_nacimiento);
        viewHolder.fechaNacTxt = (TextView) rootView.findViewById(R.id.fecha_nacimiento);
        viewHolder.lblMatricula = (TextView) rootView.findViewById(R.id.lblMatricula);
        viewHolder.tipoAutoTxt = (TextView) rootView.findViewById(R.id.tipoAuto);
        viewHolder.fotoMini = (ImageView) rootView.findViewById(R.id.imgFantasmaID);
        viewHolder.pay = (Button) rootView.findViewById(R.id.buyWallet);

        rootView.setTag(viewHolder);

        fechaVen = (TextView) rootView.findViewById(R.id.fecha_vencimiento);
        poliza = (TextView) rootView.findViewById(R.id.polizaValidaTxt);
        img = (ImageView) rootView.findViewById(R.id.estado_img);
        layOutValid = (RelativeLayout) rootView.findViewById(R.id.layOutValid);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 5;
        Bitmap preview_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bolivia_flag, options);

//		viewHolder.contenedor.setBackground(new BitmapDrawable(preview_bitmap));
        fechaV = DataActivity.fechaVencimientoAuto;

        viewHolder.pay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (fechaVen.getText().toString().equals("31/12/2017")) {
                    DialogNoActualizar("31/12/2017");
                } else {
                    if (DateUtils.validarFechaNoviembre(fechaVen.getText().toString())) {
                        //Si es verdadero no se actualiza
                        DialogNoActualizar("31/12/2017");
                    } else {
                        DIalog("2017-12-31");
                    }

                }
            }
        });

        ImageView fotoMini = (ImageView) rootView.findViewById(R.id.imgFantasmaID);
        fotoMini.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                iConta++;
                Log.d("iConta", String.valueOf(iConta));
                if (iConta == 6) {
                    //new insertDateTask().execute(DataActivity.intIDAuto, "2015-12-31");
                    insertNewDate(DataActivity.intIDAuto, "2015-12-31");
                    iConta = 0;
                    Toast.makeText(getActivity(), "Su póliza se ha vencido", Toast.LENGTH_SHORT).show();

                    layOutValid.setBackgroundColor(new Color().rgb(203, 32, 32));
                    img.setImageResource(R.drawable.vencido);
                    poliza.setText("Póliza Vencida");
                }
            }
        });

        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity.getApplicationContext();
        Log.d("FrenteFragment", "Contexto asignado");
    }

    static class ViewHolder {
        LinearLayout contenedor;
        ImageView fotoMini;
        TextView cedulaLbl;
        TextView cedulaTxt;
        TextView nombreLbl;
        TextView nombreTxt;
        TextView apellidosLbl;
        TextView apellidosTxt;
        TextView profesionLbl;
        TextView profesionTxt;
        TextView estadoCivilLbl;
        TextView estadoCivilTxt;
        TextView fechaNacLbl;
        TextView fechaNacTxt;
        TextView lugarNacLbl;
        TextView lugarNacTxt;
        TextView domicilioTxt;
        TextView domicilioLbl;
        TextView lblMatricula;
        TextView tipoAutoTxt;
        TextView tipoAuto;
        ImageView foto;
        Button pay;
    }

    public void DIalog(final String date) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set title
        alertDialogBuilder.setTitle("Renovación de Póliza");

        // set dialog message
        alertDialogBuilder.setMessage(Html.fromHtml(
                "<p align=justify>La renovación de la póliza tiene un costo de <b>Bs1.00</b> y la fecha de vencimiento de su póliza se actualizará al <b>" + date + "</b></p>"));
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Renovar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        //new insertDateTask().execute(DataActivity.intIDAuto, date);
                        insertNewDate(DataActivity.intIDAuto, date);

                        poliza.setText("Póliza Vigente");
                        img.setImageResource(R.drawable.ok);
                        layOutValid.setBackgroundColor(new Color().rgb(28, 191, 49));
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }

    public void DialogNoActualizar(String date) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set title
        alertDialogBuilder.setTitle("Cambios en su Póliza");

        // set dialog message
        alertDialogBuilder.setMessage(Html.fromHtml("Su póliza tiene fecha de vencimiento <b>" + date + "</b>. No es necesario actualizar"));
//		alertDialogBuilder.setMessage("Su póliza tiene fecha de vencimiento "+date+". No es necesario actualizar");
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }

    private void insertNewDate(final String strFolio, final String newDate) {
        JSONObject js = new JSONObject();
        try {
            js.put("strFolio", strFolio);
            js.put("dtmFechaExpiracion", newDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, URL, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(null, response.toString());
                        String [] fecha = newDate.split("-");
                        String fechaVencimientoTxt = fecha[2]+"/"+fecha[1]+"/"+fecha[0];
                        fechaVen.setText(fechaVencimientoTxt);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(null, "Error: " + error.getMessage());
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        // Adding request to request queue
        VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
    }
}
