package com.neology.soat_mifare.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.neology.soat_mifare.Grabar_UID;
import com.neology.soat_mifare.R;

/**
 * Created by root on 9/09/16.
 */
public class Record_Dialog extends DialogFragment {
    String UID;
    String tipoPoliza;

    public static Record_Dialog newInstance(String UID, String tipoPoliza) {
        Record_Dialog f = new Record_Dialog();
        Bundle args = new Bundle();
        args.putString("UID", UID);
        args.putString("poliza", tipoPoliza);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UID = getArguments().getString("UID");
        tipoPoliza = getArguments().getString("poliza");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.record_dialog, null);

        return new AlertDialog.Builder(getContext())
                .setTitle("RE-GRABAR UID")
                .setIcon(R.mipmap.logo)
                .setView(v)
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((Grabar_UID)getActivity()).regrabarUID(UID,tipoPoliza);
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }
}
