package com.neology.soat_mifare.dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.neology.soat_mifare.R;
import com.neology.soat_mifare.utils.Constants_Settings_ip;

/**
 * Created by cesar on 9/24/15.
 */
public class Dialog_Settings_ip extends DialogFragment {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    TextInputLayout textUrl;
    TextInputLayout textUsr;
    TextInputLayout textPwd;

    EditText ip;
    EditText usr;
    EditText psw;

    Button ok;
    Button cancel;

    public static Dialog_Settings_ip newInstance() {
        Dialog_Settings_ip dialog_settings = new Dialog_Settings_ip();
        return dialog_settings;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog_MinWidth);
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(Constants_Settings_ip.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings_dialog_ip, container, false);
        initElements(v);
        return v;
    }

    private void initElements(View v) {

        textUrl = (TextInputLayout) v.findViewById(R.id.textInputAddressID);
        textUsr = (TextInputLayout) v.findViewById(R.id.textInputAddressID);
        textPwd = (TextInputLayout) v.findViewById(R.id.textInputAddressID);

        ip = (EditText) v.findViewById(R.id.ipAddress);
        usr = (EditText) v.findViewById(R.id.usr);
        psw = (EditText) v.findViewById(R.id.psw);

        ok = (Button) v.findViewById(R.id.okSettingsBtnId);
        okAction();
        cancel = (Button) v.findViewById(R.id.cancelSettingsBtnId);
        cancelSettings();
    }

    private void okAction() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ip != null && ip.getText() != null && !ip.getText().toString().isEmpty() &&
                        usr != null && usr.getText() != null && !usr.getText().toString().isEmpty() &&
                        psw != null && psw.getText() != null && !psw.getText().toString().isEmpty()) {
                    if (usr.getText().toString().equals("NEOLOGY") &&
                            psw.getText().toString().equals("NeologyNFC")) {
                        editor.putString(Constants_Settings_ip.KEY_URL, ip.getText().toString());
                        editor.commit();
                        Toast.makeText(getContext(), getString(R.string.settings_set), Toast.LENGTH_SHORT).show();
                        dismiss();
                        //((DataActivity) getActivity()).ok();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.validateUsrPwd), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.all_fields_req), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void cancelSettings() {
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
