package com.neology.soat_mifare.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.neology.soat_mifare.MainActivity;
import com.neology.soat_mifare.R;

/**
 * Created by root on 9/09/16.
 */
public class FolioData_Dialog extends DialogFragment {
    int position;

    TextView folio, placa, poliza, marca, submarca;
    String [] strfolio, strplaca, strpoliza, strmarca, strsubmarca;

    public static FolioData_Dialog newInstance(int position) {
        FolioData_Dialog f = new FolioData_Dialog();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.folio_data_dialog, null);
        folio = (TextView)v.findViewById(R.id.folioExID);
        placa = (TextView)v.findViewById(R.id.placaExID);
        poliza = (TextView)v.findViewById(R.id.polizaExID);
        marca = (TextView)v.findViewById(R.id.marcaExID);
        submarca = (TextView)v.findViewById(R.id.subMarcaExID);

        strfolio = getActivity().getResources().getStringArray(R.array.folios);
        strplaca = getActivity().getResources().getStringArray(R.array.placas);
        strpoliza = getActivity().getResources().getStringArray(R.array.polizas);
        strmarca = getActivity().getResources().getStringArray(R.array.marcas);
        strsubmarca = getActivity().getResources().getStringArray(R.array.submarcas);

        folio.setText(strfolio[position]);
        placa.setText(strplaca[position]);
        switch (Integer.parseInt(strpoliza[position])) {
            case 1:
                poliza.setText("PARTICULAR");
                break;
            case 2:
                poliza.setText("MOTOCICLETA");
                break;
            case 3:
                poliza.setText("TRANSPORTE PÚBLICO");
                break;

        }
        marca.setText(strmarca[position]);
        submarca.setText(strsubmarca[position]);

        return new AlertDialog.Builder(getContext())
                .setTitle("DATOS PERSO")
                .setIcon(R.mipmap.logo)
                .setView(v)
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }
}
