package com.neology.soat_mifare.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Auto implements Parcelable {

    private String strFolio;
    private String strMarca;
    private String strSubMarca;
    private String strPlaca;
    private String strAnioModelo;
    private String dtmFechaExpiracion;
    private String strTipoAuto;
    private int intIdAuto;

    public Auto(String strFolio,
                String strMarca,
                String strSubMarca,
                String strPlaca,
                String strAnioModelo,
                String dtmFechaExpiracion,
                String strTipoAuto,
                int intIdAuto) {
        this.setStrFolio(strFolio);
        this.setStrMarca(strMarca);
        this.setStrSubMarca(strSubMarca);
        this.setStrPlaca(strPlaca);
        this.setStrAnioModelo(strAnioModelo);
        this.setDtmFechaExpiracion(dtmFechaExpiracion);
        this.setStrTipoAuto(strTipoAuto);
        this.setIntIdAuto(intIdAuto);

    }

    protected Auto(Parcel in) {
        setStrFolio(in.readString());
        setStrMarca(in.readString());
        setStrSubMarca(in.readString());
        setStrPlaca(in.readString());
        setStrAnioModelo(in.readString());
        setDtmFechaExpiracion(in.readString());
        setStrTipoAuto(in.readString());
        setIntIdAuto(in.readInt());
    }

    public static final Creator<Auto> CREATOR = new Creator<Auto>() {
        @Override
        public Auto createFromParcel(Parcel in) {
            return new Auto(in);
        }

        @Override
        public Auto[] newArray(int size) {
            return new Auto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getStrFolio());
        dest.writeString(getStrMarca());
        dest.writeString(getStrSubMarca());
        dest.writeString(getStrPlaca());
        dest.writeString(getStrAnioModelo());
        dest.writeString(getDtmFechaExpiracion());
        dest.writeString(getStrTipoAuto());
        dest.writeInt(getIntIdAuto());
    }


    public String getStrFolio() {
        return strFolio;
    }

    public void setStrFolio(String strFolio) {
        this.strFolio = strFolio;
    }

    public String getStrMarca() {
        return strMarca;
    }

    public void setStrMarca(String strMarca) {
        this.strMarca = strMarca;
    }

    public String getStrSubMarca() {
        return strSubMarca;
    }

    public void setStrSubMarca(String strSubMarca) {
        this.strSubMarca = strSubMarca;
    }

    public String getStrPlaca() {
        return strPlaca;
    }

    public void setStrPlaca(String strPlaca) {
        this.strPlaca = strPlaca;
    }

    public String getStrAnioModelo() {
        return strAnioModelo;
    }

    public void setStrAnioModelo(String strAnioModelo) {
        this.strAnioModelo = strAnioModelo;
    }

    public String getDtmFechaExpiracion() {
        return dtmFechaExpiracion;
    }

    public void setDtmFechaExpiracion(String dtmFechaExpiracion) {
        this.dtmFechaExpiracion = dtmFechaExpiracion;
    }

    public String getStrTipoAuto() {
        return strTipoAuto;
    }

    public void setStrTipoAuto(String strTipoAuto) {
        this.strTipoAuto = strTipoAuto;
    }

    public int getIntIdAuto() {
        return intIdAuto;
    }

    public void setIntIdAuto(int intIdAuto) {
        this.intIdAuto = intIdAuto;
    }
}
