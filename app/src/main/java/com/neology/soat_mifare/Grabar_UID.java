package com.neology.soat_mifare;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.neology.soat_mifare.dialogs.FolioData_Dialog;
import com.neology.soat_mifare.dialogs.Record_Dialog;
import com.neology.soat_mifare.dialogs.Settings_Dialog;
import com.neology.soat_mifare.utils.Constants;
import com.nxp.nfclib.classic.IMFClassic;
import com.nxp.nfclib.classic.IMFClassicEV1;
import com.nxp.nfclib.exceptions.CloneDetectedException;
import com.nxp.nfclib.exceptions.ReaderException;
import com.nxp.nfclib.exceptions.SmartCardException;
import com.nxp.nfclib.icode.IICodeSLI;
import com.nxp.nfclib.icode.IICodeSLIL;
import com.nxp.nfclib.icode.IICodeSLIS;
import com.nxp.nfclib.icode.IICodeSLIX;
import com.nxp.nfclib.icode.IICodeSLIX2;
import com.nxp.nfclib.icode.IICodeSLIXL;
import com.nxp.nfclib.icode.IICodeSLIXS;
import com.nxp.nfclib.ntag.INTAGI2Cplus;
import com.nxp.nfclib.ntag.INTag;
import com.nxp.nfclib.ntag.INTag203x;
import com.nxp.nfclib.ntag.INTag210;
import com.nxp.nfclib.ntag.INTag213215216;
import com.nxp.nfclib.ntag.INTag213F216F;
import com.nxp.nfclib.ntag.INTagI2C;
import com.nxp.nfclib.plus.IPlusSL1;
import com.nxp.nfclib.ultralight.IUltralight;
import com.nxp.nfclib.ultralight.IUltralightC;
import com.nxp.nfclib.ultralight.IUltralightEV1;
import com.nxp.nfclib.utils.NxpLogUtils;
import com.nxp.nfclib.utils.Utilities;
import com.nxp.nfcliblite.Inxpnfcliblitecallback;
import com.nxp.nfcliblite.NxpNfcLibLite;
import com.nxp.nfcliblite.Nxpnfcliblitecallback;
import com.nxp.nfcliblite.cards.IDESFireEV1;
import com.nxp.nfcliblite.cards.IPlus;

import java.io.IOException;

public class Grabar_UID extends AppCompatActivity {
    public static String TAG = Grabar_UID.class.getSimpleName();
    private NxpNfcLibLite libInstance = null;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    Button mostrarUID;
    Spinner poliza;

    int iConta_UID = 0;
    String tipoPoliza;

    /**
     * Mifare MFClassic instance initiated.
     */
    private IMFClassic classic;
    /**
     * Mifare Plus instance initiated.
     */
    private IPlus plus;

    private enum EnumCardType {
        EnumUltraLight,
        EnumClassic,
        EnumUltraLightC,
        EnumUltraLightEV1,
        EnumPlus,
        EnumPlusSL1,
        EnumNTag203x,
        EnumNTag210,
        EnumNTag213215216,
        EnumNTag213F216F,
        EnumNTagI2C,
        EnumNTagI2CPLUS,
        EnumICodeSLI,
        EnumICodeSLIS,
        EnumICodeSLIL,
        EnumICodeSLIX,
        EnumICodeSLIXS,
        EnumICodeSLIXL,
        EnumICodeSLIX2,
        EnumDESFireEV1,
        EnumPlusX,
        EnumPlusS,
        EnumClassicEV1,
        EnumNone

    }

    private EnumCardType mCardType = EnumCardType.EnumNone;

    private boolean mIsPerformingCardOperations = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabar__uid);
        libInstance = NxpNfcLibLite.getInstance();
        // Call registerActivity function before using other functions of the library.
        libInstance.registerActivity(this);

        sharedPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_NAME, Context.MODE_PRIVATE);

        if (!sharedPreferences.contains("Contador")) {
            editor = sharedPreferences.edit();
            editor.putInt("Contador", 0);
            editor.commit();
            Log.i(TAG, "Contador ingresado en 0");
        }

        loadWebView();
        loadSpinner();
        mostrarUID();
    }

    private void loadSpinner() {
        poliza = (Spinner) findViewById(R.id.polizaID);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.folios, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        poliza.setAdapter(adapter);
        poliza.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipoPoliza = parent.getItemAtPosition(position).toString();
                showDialogFolio(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "nada seleccionado, poliza 1 por default");
            }
        });
    }

    private void showDialogFolio(int position) {
        DialogFragment dialogFragment = FolioData_Dialog.newInstance(position);
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void loadWebView() {
        WebView wv = (WebView) findViewById(R.id.webView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/tap.html");
    }

    private void mostrarUID() {
        mostrarUID = (Button) findViewById(R.id.mostrarUID);
        mostrarUID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.contains("Contador") || sharedPreferences.getInt("Contador", 0) != 0) {
                    Log.i(TAG, "UID " + sharedPreferences.getAll());
                } else {
                    Toast.makeText(getApplicationContext(), "No existen UID's Registrados", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grabar__uid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }

    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        try {
            libInstance.filterIntent(intent, new Nxpnfcliblitecallback() {
                @Override
                public void onClassicEV1CardDetected(final IMFClassicEV1 imfClassicEV1) {
                    if (mCardType == EnumCardType.EnumClassicEV1 && mIsPerformingCardOperations) {
                        //Already Some Operations are happening in the same card, discard the callback
                        Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                        return;
                    }
                    mIsPerformingCardOperations = true;
                    mCardType = EnumCardType.EnumClassicEV1;


                    classic = imfClassicEV1;
                /* Insert your logic here by commenting the function call below. */
                    try {
                        classic.getReader().connect();
                        classicEV1CardLogic();
                    } catch (Throwable t) {
                        Log.e(TAG, "Unknown Error Tap Again!");
                    }

                    mIsPerformingCardOperations = false;
                }

                @Override
                public void onPlusCardDetected(final IPlus iPlus) {
                    if (mCardType == EnumCardType.EnumPlus && mIsPerformingCardOperations) {
                        //Already Some Operations are happening in the same card, discard the callback
                        Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                        return;
                    }
                    mIsPerformingCardOperations = true;
                    mCardType = EnumCardType.EnumPlus;

                    plus = iPlus;
                    try {
                        plus.getReader().connect();
                        plusCardLogic();
                    } catch (Throwable t) {
                        t.printStackTrace();
                        Log.e(TAG, "Unknown Error Tap Again!");
                    }

                    mIsPerformingCardOperations = false;
                }

                @Override
                public void onNTag213215216CardDetected(final INTag213215216 inTag213215216) {
                    if (mCardType == EnumCardType.EnumNTag213215216 && mIsPerformingCardOperations) {
                        //Already Some Operations are happening in the same card, discard the callback
                        Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                        return;
                    }
                    mIsPerformingCardOperations = true;
                    mCardType = EnumCardType.EnumNTag213215216;

                    try {
                        inTag213215216.getReader().connect();
                        ntagCardLogic(inTag213215216);
                    } catch (ReaderException e) {

                        e.printStackTrace();
                    }


                    mIsPerformingCardOperations = false;
                }
            });
        } catch (CloneDetectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Plus lite operations.
     *
     * @throws SmartCardException when exception occur.
     */
    public void plusCardLogic() throws SmartCardException {

        try {
            Log.i(TAG, "Card Detected: " + plus.getCardDetails().cardName + " "
                    + plus.getCardDetails().securityLevel + Utilities.dumpBytes(plus.getCardDetails().uid));
            createShared(Utilities.dumpBytes(plus.getCardDetails().uid));
            plus.getReader().close();
        } catch (ReaderException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Mifare classic Card Logic.
     *
     * @throws SmartCardException
     */
    public void classicEV1CardLogic() throws SmartCardException {
        Log.i(TAG, "Card Detected : " + classic.getCardDetails().cardName);

        try {
            Log.i(TAG, "Uid :" + Utilities.dumpBytes(classic.getUID()));
            createShared(Utilities.dumpBytes(classic.getUID()));
            classic.getReader().close();
        } catch (ReaderException e) {

            e.printStackTrace();
        }
    }

    /**
     * Ntag Operations are, getTagname(), getUID(), Write and Read.
     *
     * @param tag object
     */
    private void ntagCardLogic(final INTag tag) {
        Log.i(TAG, "Card Detected : " + tag.getTagName());

        try {
            NxpLogUtils.d(TAG, "testBasicNtagFunctionality, start");

            Log.i(TAG, "UID of the Tag: " + Utilities.dumpBytes(tag.getUID()));
            createShared(Utilities.dumpBytes(tag.getUID()));
            tag.getReader().close();
            NxpLogUtils.d(TAG, "testBasicNtagFunctionality, End");
        } catch (TagLostException e) {
            Log.e(TAG, "TagLost Exception - Tap Again!");
            e.printStackTrace();
        } catch (IOException e) {

            Log.e(TAG, "IO Exception -  Check logcat!");
            e.printStackTrace();
        } catch (SmartCardException e) {
            Log.e(TAG, "SmartCard Exception - Check logcat!");
            e.printStackTrace();
        } catch (Throwable t) {
            Log.e(TAG, "Exception - Check logcat!");
            t.printStackTrace();
        }
    }

    private void createShared(String UID) {
        //verificamos si hay uid registrado
        if (!sharedPreferences.contains(UID)) {
            editor = sharedPreferences.edit();
            editor.putString(UID, UID + "|" + tipoPoliza);
            editor.commit();
            Toast.makeText(getApplicationContext(), UID + " registrado", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), UID + " registrado anteriormente", Toast.LENGTH_SHORT).show();
            showDialog(UID, tipoPoliza );
        }
    }

    void showDialog(String UID, String tipoPoliza) {
        DialogFragment dialogFragment = Record_Dialog.newInstance(UID, tipoPoliza);
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void regrabarUID(String UID, String tipoPoliza){
        editor = sharedPreferences.edit();
        editor.remove(UID);
        editor.commit();
        editor.putString(UID, UID +"|"+tipoPoliza);
        editor.commit();
        Toast.makeText(getApplicationContext(), UID + " registrado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
