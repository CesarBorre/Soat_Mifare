package com.neology.soat_mifare;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neology.soat_mifare.model.Auto;
import com.neology.soat_mifare.pagetransformers.ZoomOutPageTransformer;
import com.neology.soat_mifare.subfragments.FrenteFragment;
import com.neology.soat_mifare.utils.CheckInternetConnection;
import com.neology.soat_mifare.utils.DateUtils;

public class DataActivity extends FragmentActivity implements
        ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    private final String TAG = DataActivity.class.getSimpleName();
    private String no_cedula ="", tipoPoliza = "", polizaValida="";
    private String datos[];

    private static final String URL = "http://mobile.neology-demos.com:8080//api/autos_soat?strFolio=";

    public static String intIDAuto= "", fechaVencimientoAuto = "";

    // Dialog to show a wait message
    private ProgressDialog dialog;
    // Dialog to show at download proccess
    private ProgressDialog dialogDownload;

    // Defined to UI values
    private ImageView ivUI;

    Button pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        Intent intent = getIntent();

        String d = intent.getStringExtra("NO_CEDULA");
        datos = d.split("\\|");
        no_cedula = datos[0];
        tipoPoliza = datos[1];
        polizaValida = datos[2];

        // Creacion del mensaje de espera
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_label));
        dialog.setTitle(getString(R.string.gettingdata_label));
        dialog.setCanceledOnTouchOutside(false);

        dialogDownload = new ProgressDialog(this);
        dialogDownload.setMessage(getString(R.string.wait_label));
        dialogDownload.setTitle(getString(R.string.gettinimage_label));
        dialogDownload.setIndeterminate(false);
        dialogDownload.setMax(100);
        dialogDownload.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogDownload.setCanceledOnTouchOutside(false);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Set up custom animation
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
//			new ConsultaWS().execute(no_cedula);
            callRest(no_cedula);
            //new ws().execute(no_cedula);
        } else {
            Toast.makeText(getApplicationContext(), "No hay servicio de Internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.data, menu);
        return true;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab,
                              FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab,
                                FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab,
                                FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new FrenteFragment();
                    break;
                case 1:
//				fragment = new ReversoFragment();
                    break;
                case 2:
                    break;
            }
            Bundle args = new Bundle();
            args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
//				return getString(R.string.title_section2).toUpperCase(l);
                case 2:
//				return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply
     * displays dummy text.
     */
    public static class DummySectionFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public static final String ARG_SECTION_NUMBER = "section_number";

        public DummySectionFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_data_dummy,
                    container, false);
            TextView dummyTextView = (TextView) rootView
                    .findViewById(R.id.section_label);
            dummyTextView.setText(Integer.toString(getArguments().getInt(
                    ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    private void callRest(String strFolio) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL+strFolio,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        new ws().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        Toast.makeText(getApplicationContext(), "Error en la consulta, vuelva a intentar", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
        );
        VolleyApp.getmInstance().addToRequestQueue(jsonObjectRequest);
    }

    private class ws extends AsyncTask<JSONObject, Void, Auto> {

        Auto auto;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Auto doInBackground(JSONObject... params) {
            try {
                JSONObject jsonObject = (JSONObject)params[0].get("autosSoat");
                JSONObject jsonObject1 = jsonObject.getJSONObject("tipoAutoSoat");
                auto = new Auto(
                        jsonObject.getString("strFolio"),
                        jsonObject.getString("strMarca"),
                        jsonObject.getString("strSubMarca"),
                        jsonObject.getString("strPlaca"),
                        jsonObject.getString("strAnioModelo"),
                        jsonObject.getString("dtmFechaExpiracion"),
                        jsonObject1.getString("strTipoAuto"),
                        jsonObject.getInt("idtblAutos_Bol"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return auto;
        }

        @Override
        protected void onPostExecute(Auto result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result != null) {
                try {
                    setUIDatosFrente(result);
                }catch(Exception e) {

                }
            } else {

                Toast.makeText(getApplicationContext(), "Error en la consulta, vuelva a intentar", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }

    @SuppressWarnings("static-access")
    public void setUIDatosFrente(Auto auto) {
        String poliza [] = auto.getStrFolio().split("_");


        TextView folio = (TextView)findViewById(R.id.nombre);
        if(poliza[1].equals("M")) {
            folio.setText(poliza[1]+" "+poliza[2]);
        } else {
            folio.setText(poliza[1]);
        }

        TextView marca = (TextView)findViewById(R.id.apellidos);
        marca.setText(auto.getStrMarca());

        TextView subMarca = (TextView)findViewById(R.id.profesion);
        subMarca.setText(auto.getStrSubMarca());

        TextView placa = (TextView)findViewById(R.id.estado_civil);
        placa.setText(auto.getStrPlaca());

        TextView anioModelo = (TextView)findViewById(R.id.fecha_nacimiento);
        anioModelo.setText(auto.getStrAnioModelo());

        TextView fechaVencimiento = (TextView)findViewById(R.id.fecha_vencimiento);
        String [] fecha = auto.getDtmFechaExpiracion().split("-");
        String fechaVencimientoTxt = fecha[2]+"/"+fecha[1]+"/"+fecha[0];
        fechaVencimiento.setText(fechaVencimientoTxt);

        ImageView v  = (ImageView)findViewById(R.id.foto_img);
        ImageView v1 = (ImageView)findViewById(R.id.imgFantasmaID);

        TextView matricula = (TextView)findViewById(R.id.txtCedulaID);

        TextView tipoPolizaTxt = (TextView)findViewById(R.id.lblMatricula);
        tipoPolizaTxt.setText(auto.getStrTipoAuto());

        TextView tipo = (TextView)findViewById(R.id.tipoAuto);

        switch (Integer.parseInt(tipoPoliza)) {
            case 1:
                v.setImageResource(R.drawable.bmw);
                v1.setImageResource(R.drawable.soat5);
                matricula.setText("65000088961234567");
                tipo.setText("PARTICULAR");
                break;

            case 2:
                v.setImageResource(R.drawable.iron);
                v1.setImageResource(R.drawable.soat4);
                matricula.setText("65000088977654321");
                tipo.setText("MOTOCICLETA");
                break;

            case 3:
                v.setImageResource(R.drawable.taxi);
                v1.setImageResource(R.drawable.verde_1);
                matricula.setText("65000088981325463");
                tipo.setText("TAXI");
                break;
        }


        if (auto.getStrFolio().equals("85923797A-13")|| poliza[0].equals("P")) {
            matricula.setText("WAPB7TL014B710051");
        } else if (auto.getStrFolio().equals("85923797A-18")||poliza[0].equals("T")) {
            matricula.setText("WVWMA63B2WE307907");
        } else if (auto.getStrFolio().equals("85923797A-14")||poliza[0].equals("M")) {
            matricula.setText("1HD4CAM104K400506");
        }

        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.layOutValid);

        ImageView ivUI = (ImageView) findViewById(R.id.estado_img);
        TextView polizaVigenteLbl = (TextView)findViewById(R.id.polizaValidaTxt);

        polizaValida = DateUtils.validarFechaPoliza(auto.getDtmFechaExpiracion());
        Log.d("Fecha Poliza", polizaValida);

        if (polizaValida.equals("OK")) {
            relativeLayout.setBackgroundColor(new Color().rgb(28, 191, 49));
            ivUI.setImageResource(R.drawable.ok);
            polizaVigenteLbl.setText("Póliza Vigente");

        } else {
            relativeLayout.setBackgroundColor(new Color().rgb(203, 32, 32));
            ivUI.setImageResource(R.drawable.vencido);
            polizaVigenteLbl.setText("Póliza Vencida");
        }

        intIDAuto = auto.getStrFolio();
        fechaVencimientoAuto = auto.getDtmFechaExpiracion();

    }

}
